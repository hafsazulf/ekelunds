const button = document.querySelector('.toggle-button');
const hiddenItems = document.querySelectorAll('.hidden-item');
let isHidden = true;
button.addEventListener('click', () => {

  isHidden = !isHidden;
  hiddenItems.forEach(item => item.classList.toggle('hidden'));
});

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	const buttone = document.querySelector('.toggle-button');
const hiddenItemse = document.querySelectorAll('.hidden-item');
let isHidden = true;
buttone.addEventListener('touchstart', () => {

  isHidden = !isHidden;
  hiddenItemse.forEach(item => item.classList.toggle('hidden'));
});
}