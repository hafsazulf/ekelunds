;define(
    [
        'jquery',
        'Magento_Ui/js/modal/modal',
        'text!Magento_Catalog/templates/Got_popup.html',
    ],
    function($) {
        "use strict";
        //creating jquery widget
        $.widget('Gots.Popup', {
            options: {
                modalForm: '#popup',
                modalButton: '.popup-open'
            },
            _create: function() {
                this.options.modalOption = this.getModalOptions();
                this._bind();
            },
            getModalOptions: function() {
                /** * Modal options */
                var options = {
                    type: 'popup',
                    responsive: true,
                    clickableOverlay: false,
                    title: $.mage.__('Global Organic Textile Standard (GOTS) är en världsledande standard för organiska fibrer, inklusive ekologiska och sociala kriterier. Syftet med standarden är att definiera de krav som säkerställer ekologisk status av textilier, från skörd av råmaterial, genom miljömässigt och socialt ansvarsfull tillverkning en trovärdig försäkran till slutkonsumenten.'),
                    modalClass: 'popup',
                    buttons: [{
                        text: $.mage.__('Ok!'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };
                return options;
            },
            _bind: function(){
                var modalOption = this.options.modalOption;
                var modalForm = this.options.modalForm;
                $(document).on('click', this.options.modalButton, function(){
                    $(modalForm).modal(modalOption);
                    $(modalForm).trigger('openModal');
                });
            }
        });

        return $.Gots.Popup;
    }
);