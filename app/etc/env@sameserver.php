<?php
return [
    'backend' => [
        'frontName' => 'admin'
    ],
    'crypt' => [
        'key' => 'fa4fcaf67a683d9618578e462fe3ad8b'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => '/opt/bitnami/mysql/tmp/mysql.sock',
                'dbname' => 'ekelund',
                'username' => 'ekelund',
                'password' => '^V5J&wAf',
                'active' => '1',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'default',
    'session' => [
        'save' => 'files'
    ],
    'cache' => [
        'frontend' => [
            'default' => [
                'id_prefix' => 'c25_'
            ],
            'page_cache' => [
                'id_prefix' => 'c25_'
            ]
        ]
    ],
    'lock' => [
        'provider' => 'db',
        'config' => [
            'prefix' => ''
        ]
    ],
    'install' => [
        'date' => 'Fri, 28 Feb 2020 07:59:06 +0000'
    ],
    'cache_types' => [
        'compiled_config' => 1,
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'google_product' => 1,
        'full_page' => 1,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1
    ],
    'system' => [
        'default' => [
            'dev' => [
                'js' => [
                    'enable_js_bundling' => '0',
                    'minify_files' => '0',
                    'merge_files' => '0'
                ],
                'css' => [
                    'minify_files' => '0'
                ]
            ]
        ]
    ],
    'downloadable_domains' => [
        '18.158.142.106',
        'd2bc0e1yjc0xnq.cloudfront.net'
    ]
];
