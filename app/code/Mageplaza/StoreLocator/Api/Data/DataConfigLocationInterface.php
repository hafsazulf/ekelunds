<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreLocator
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\StoreLocator\Api\Data;

/**
 * Interface DataConfigLocationInterface
 * @package Mageplaza\StoreLocator\Api\Data
 */
interface DataConfigLocationInterface
{
    const ZOOM                          = 'zoom';
    const MARKER_ICON                   = 'markerIcon';
    const DATA_LOCATIONS                = 'dataLocations';
    const INFO_WINDOW_TEMPLATE_PATH     = 'infowindowTemplatePath';
    const LIST_TEMPLATE_PATH            = 'listTemplatePath';
    const KML_INFO_WINDOW_TEMPLATE_PATH = 'KMLinfowindowTemplatePath';
    const KML_LIST_TEMPLATE_PATH        = 'KMLlistTemplatePath';
    const IS_FILTER                     = 'isFilter';
    const IS_FILTER_RADIUS              = 'isFilterRadius';
    const LOCATION_ID_DETAIL            = 'locationIdDetail';
    const URL_SUFFIX                    = 'urlSuffix';
    const KEY_MAP                       = 'keyMap';
    const ROUTER                        = 'router';
    const IS_DEFAULT_STORE              = 'isDefaultStore';
    const DEFAULT_LAT                   = 'defaultLat';
    const DEFAULT_LNG                   = 'defaultLng';
    const LOCATIONS_DATA                = 'locationsData';

    /**
     * @return string
     */
    public function getZoom();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setZoom($value);

    /**
     * @return string
     */
    public function getMarkerIcon();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setMarkerIcon($value);

    /**
     * @return string
     */
    public function getDataLocations();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setDataLocations($value);

    /**
     * @return string
     */
    public function getInfowindowTemplatePath();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setInfowindowTemplatePath($value);

    /**
     * @return string
     */
    public function getListTemplatePath();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setListTemplatePath($value);

    /**
     * @return string
     */
    public function getKmlInfowindowTemplatePath();

    /**
     * @param string $value
     *
     * @return string
     */
    public function setKmlInfowindowTemplatePath($value);

    /**
     * @return string
     */
    public function getKmlListTemplatePath();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setKmlListTemplatePath($value);

    /**
     * @return string
     */
    public function getIsFilter();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setIsFilter($value);

    /**
     * @return string
     */
    public function getIsFilterRadius();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setIsFilterRadius($value);

    /**
     * @return string
     */
    public function getLocationIdDetail();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setLocationIdDetail($value);

    /**
     * @return string
     */
    public function getUrlSuffix();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setUrlSuffix($value);

    /**
     * @return string
     */
    public function getKeyMap();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setKeyMap($value);

    /**
     * @return string
     */
    public function getRouter();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setRouter($value);

    /**
     * @return string
     */
    public function getIsDefaultStore();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setIsDefaultStore($value);

    /**
     * @return string
     */
    public function getDefaultLat();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setDefaultLat($value);

    /**
     * @return string
     */
    public function getDefaultLng();

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setDefaultLng($value);

    /**
     * @return \Mageplaza\StoreLocator\Api\Data\LocationDataInterface[]
     */
    public function getLocationsData();

    /**
     * @param \Mageplaza\StoreLocator\Api\Data\LocationDataInterface[] $value
     *
     * @return $this
     */
    public function setLocationsData($value);
}
