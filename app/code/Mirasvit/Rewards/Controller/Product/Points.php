<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   2.3.38
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Rewards\Controller\Product;

use Magento\Framework\Controller\ResultFactory;

class Points extends \Mirasvit\Rewards\Controller\Product
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface|string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Currency_Exception
     */
    public function execute()
    {
        $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $response = '';

        $data = $this->getRequest()->getParams();

        $product         = $this->productRepository->getById((int)$data['product_id']);
        $productPrice    = (float)$data['price'] * (int)$data['qty'];
        $customerGroupId = $this->customerSession->getCustomerGroupId();

        $points = $this->earnHelper->getProductPoints($product, $customerGroupId, 0, $productPrice);
        if ($this->config->getGeneralIsDisplayProductPointsAsMoney()) {
            $customer = $this->customerSession->getCustomer();
            $websiteId = $this->storeManager->getWebsite()->getId();

            $money = $this->spendHelper->getProductPointsAsMoney(
                $points, $websiteId, $customerGroupId, $productPrice, $customer);
            $label = __('Possible discount %1', $money);
        } else {
            $label = __('Earn %1', $this->rewardsDataHelper->formatPoints($points));
        }

        $result = [
            'points' => $points,
            'label'  => $label,
        ];
        if ($this->getRequest()->isXmlHttpRequest()) {
            $resultJson = $this->resultJsonFactory->create();
            $response = $resultJson->setData($result);
        }
        return $response;
    }
}
