<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   2.3.38
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Rewards\Controller;

use Magento\Framework\App\Action\Action;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class Product extends Action
{
    protected $config;
    protected $customerSession;
    protected $earnHelper;
    protected $productRepository;
    protected $resultJsonFactory;
    protected $rewardsDataHelper;
    protected $spendHelper;
    protected $storeManager;

    public function __construct(
        \Mirasvit\Rewards\Helper\Data                    $rewardsDataHelper,
        \Mirasvit\Rewards\Helper\Balance\Earn            $earnHelper,
        \Mirasvit\Rewards\Helper\Balance\Spend           $spendHelper,
        \Mirasvit\Rewards\Model\Config                   $config,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface  $productRepository,
        \Magento\Customer\Model\Session                  $customerSession,
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        \Magento\Framework\App\Action\Context            $context
    ) {
        $this->config            = $config;
        $this->rewardsDataHelper = $rewardsDataHelper;
        $this->earnHelper        = $earnHelper;
        $this->spendHelper       = $spendHelper;
        $this->productRepository = $productRepository;
        $this->customerSession   = $customerSession;
        $this->storeManager      = $storeManager;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }
}
