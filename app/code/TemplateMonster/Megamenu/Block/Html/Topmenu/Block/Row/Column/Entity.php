<?php

namespace TemplateMonster\Megamenu\Block\Html\Topmenu\Block\Row\Column;

use Magento\Framework\View\Element\Template;

class Entity extends Template
{
    protected $_template = 'html/topmenu/block/row/column/default.phtml';
	
	protected $_catmodel;

    public function __construct(
        Template\Context $context,
		\Ekelund\Common\Model\Catmodel $catmodel,
        array $data = []
    ) {
		$this->_catmodel = $catmodel;
        parent::__construct($context, $data);
    }

    public function renderEntity($entity)
    {
        $this->setEntity($entity);

        return $this->toHtml();
    }
}