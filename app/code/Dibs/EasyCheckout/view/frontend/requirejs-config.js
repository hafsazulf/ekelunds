var config = {
    map: {
        '*': {
            dibsEasyCheckout: 'Dibs_EasyCheckout/js/checkout',
        }
    },
    paths: {
        slick: 'Dibs_EasyCheckout/js/lib/slick.min'
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }
};