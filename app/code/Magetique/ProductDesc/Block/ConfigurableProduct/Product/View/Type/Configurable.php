<?php
namespace Magetique\ProductDesc\Block\ConfigurableProduct\Product\View\Type;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\DecoderInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;



class Configurable
{

    protected $jsonEncoder;
    protected $jsonDecoder;
    protected $stockRegistry;
    protected $productloader;
    protected $_reviewCollection;
    protected $appEmulation;
    protected $imageHelperFactory;
    protected $productRepository;
    public function __construct(
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        StockRegistryInterface $stockRegistry,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollection,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Helper\Image $imageHelperFactory,
		\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    ) {

        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->stockRegistry = $stockRegistry;
        $this->productloader = $_productloader;
        $this->_reviewCollection = $reviewCollection;
        $this->appEmulation = $appEmulation;
        $this->productRepository = $productRepository;
        $this->imageHelperFactory = $imageHelperFactory;
		$this->priceCurrency = $priceCurrency;
    }

    // Adding Quantitites (product=>qty)
    public function aroundGetJsonConfig(
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
        \Closure $proceed
    )
    {
        $descriptions = [];
        $productName = [];
        $productPrice = [];
        $gfdescriptions = [];
        $giftImage = [];
        $giftbox = [];
        $gfid = [];
        $shortdescriptions = [];
        $barcodes = [];
        $designers = [];
        $p_colors = [];
        $materials = [];
        $washing_instructions = [];
        $washing_tips = [];
        $p_reviews = [];
        $product_storys = [];
        $product_story_description = [];
        $environment_licences = [];
        $config = $proceed();
        $config = $this->jsonDecoder->decode($config);

        foreach ($subject->getAllowProducts() as $product) {
            $stockitem = $this->stockRegistry->getStockItem(
                $product->getId(),
                $product->getStore()->getWebsiteId()
            );

            $products = $this->productloader->create()->load($product->getId());


            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/magentique_desctiption.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
//            $logger->info('Magentique Desctiptions');
            $logger->info(print_r($products->debug(), true));
            $collection = $this->_reviewCollection->create()
                ->addStatusFilter(
                    \Magento\Review\Model\Review::STATUS_APPROVED
                )->addEntityFilter(
                    'product',
                    $product->getId()
                )->setDateOrder();
                
            $barcodes[$product->getId()] = $products->getBarcode();
            $designers[$product->getId()] = $products->getAttributeText('designer');
            $materials[$product->getId()] = $products->getAttributeText('material');
            $p_colors[$product->getId()] = $products->getAttributeText('color');
            $washing_instructions[$product->getId()] = $products->getWashing_instruction();
            $washing_tips[$product->getId()] = $products->getWashing_tip();

            $environment_licences[$product->getId()] = $products->getEnvironmental_licensed();
            $productImageAttr = null;
            $productImage = null;
            $gfImage = null;

            if($products->hasData('product_story'))
            {
                $productImageAttr = $products->getCustomAttribute( 'product_story' );
                $productImage = $this->imageHelperFactory->init($products, 'product_story')
                    ->setImageFile($productImageAttr->getValue());
                $product_storys[$product->getId()] = $productImage->getUrl();
            }else {
                $product_storys[$product->getId()] = null;
            }
			
            $product_story_description[$product->getId()] = $products->getProduct_story_des();
            $shortdescriptions[$product->getId()] = $products->getShortDescription();
            $descriptions[$product->getId()] = $products->getDescription();
            $productName[$product->getId()] = $products->getName();
            $giftbox[$product->getId()] = $products->getProductGiftBox();
			if(!empty($products->getProductGiftBox())){
				$giftProduct = $this->productRepository->get($products->getProductGiftBox());
				$productPrice[$product->getId()] = $this->priceCurrency->convertAndFormat($giftProduct->getPrice());
				$gfImage = $this->imageHelperFactory->init($giftProduct, 'product_page_image_small')
								->setImageFile($giftProduct->getSmallImage()) // image,small_image,thumbnail
								->resize(250)
								->getUrl();
				$giftImage[$product->getId()] = $gfImage;
				$gfdescriptions[$product->getId()] = $giftProduct->getDescription();
				$gfid[$product->getId()] = $giftProduct->getId();
			}
        }
		
        $config['barcodes'] = $barcodes;
        $config['designers'] = $designers;
        $config['materials'] = $materials;
        $config['p_colors'] = $p_colors;
        $config['washing_instructions'] = $washing_instructions;
        $config['washing_tips'] = $washing_tips;
        $config['product_storys'] = $product_storys;
        $config['product_story_description'] = $product_story_description;
        $config['shortdescriptions'] = $shortdescriptions;
        $config['descriptions'] = $descriptions;
        $config['product_name'] = $productName;
        $config['product_price'] = $productPrice;
        $config['product_image'] = $giftImage;
        $config['product_giftbox'] = $giftbox;
        $config['gf_id'] = $gfid;
        $config['product_descriptiions'] = $gfdescriptions;
        $config['environment_licences'] = $environment_licences;
		
        return $this->jsonEncoder->encode($config);
    }
}

?>