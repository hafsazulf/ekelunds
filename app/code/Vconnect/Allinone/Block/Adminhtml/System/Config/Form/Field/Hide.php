<?php
/* 
 * The MIT License
 *
 * Copyright 2016 vConnect.dk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * @category Magento
 * @package Vconnect_AllInOne
 * @author vConnect
 * @email kontakt@vconnect.dk
 * @class Prices
 */

namespace Vconnect\Allinone\Block\Adminhtml\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

class Hide extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Vconnect\Allinone\Helper\Data
     */
    protected $allinoneHelperData;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Vconnect\Allinone\Helper\Data $allinoneHelperData
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Vconnect\Allinone\Helper\Data $allinoneHelperData,
        array $data = []
    ) {
        $this->allinoneHelperData = $allinoneHelperData;

        parent::__construct(
            $context,
            $data
        );
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $fieldConfig = $element->getFieldConfig();
        $countryCode = $this->allinoneHelperData->getStoreConfig('shipping/origin/country_id');

        if ($this->allinoneHelperData->canShowConfiguration($countryCode, $fieldConfig['id'])) {
            return parent::render($element);
        } else {
            return '';
        }
    }
}

