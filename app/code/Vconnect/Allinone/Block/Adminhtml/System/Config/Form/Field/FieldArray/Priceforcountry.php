<?php
/* 
 * The MIT License
 *
 * Copyright 2016 vConnect.dk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * @category Magento
 * @package Vconnect_AllInOne
 * @author vConnect
 * @email kontakt@vconnect.dk
 * @class Prices
 */

namespace Vconnect\Allinone\Block\Adminhtml\System\Config\Form\Field\FieldArray;

use Magento\Backend\Block\Template\Context;

class Priceforcountry extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    protected $_countryRenderer;

    /**
     * @var \Vconnect\Allinone\Helper\Data
     */
    protected $allinoneHelperData;

    /**
     * Priceforcountry constructor.
     * @param Context $context
     * @param \Vconnect\Allinone\Helper\Data $allinoneHelperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Vconnect\Allinone\Helper\Data $allinoneHelperData,
        array $data = []
    ) {
        $this->allinoneHelperData = $allinoneHelperData;

        parent::__construct($context, $data);

        $this->setHtmlId('_' . uniqid());
    }

    /**
     * Retrieve group column renderer
     *
     * @return Serviceslabels|\Magento\Framework\View\Element\BlockInterface
     */
    protected function _getCountryRenderer()
    {
        if (!$this->_countryRenderer) {
            $this->_countryRenderer = $this->getLayout()->createBlock(
                'Magento\Framework\View\Element\Html\Select',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->_countryRenderer->setId('allinone_method_home_country_row_<%- _id %>');
            $this->_countryRenderer->setClass('validate-select allinone_method_home_country_row_<%- _id %>');
            $this->_countryRenderer->setName($this->getElement()->getName() . '[<%- _id %>][country]');

            $arr = array();
            if (method_exists($this, '_getCountryOptions')) {
                $arr = $this->_getCountryOptions();
            } else {
                $scandinavianCountries = $this->allinoneHelperData->getScandinavianCountries();
                foreach ($scandinavianCountries as $scandinavianCountryCode) {
                    $arr[] = array(
                        'value' => strtoupper($scandinavianCountryCode),
                        'label' => strtoupper($scandinavianCountryCode),
                    );
                }
            }

            $this->_countryRenderer->setOptions($arr);
        }
        return $this->_countryRenderer;
    }

    protected function _prepareToRender()
    {
        $this->addColumn('country', array(
            'label' => __('Country'),
            'renderer' => $this->_getCountryRenderer()
        ));
        $this->addColumn('orderminprice', array(
            'label' => __('Min Price'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));
        $this->addColumn('ordermaxprice', array(
            'label' => __('Max Price'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));
        $this->addColumn('orderminweight', array(
            'label' => __('Min Weight'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));
        $this->addColumn('ordermaxweight', array(
            'label' => __('Max Weight'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));
        $this->addColumn('price', array(
            'label' => __('Shipping Fee'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Rate');
    }


    /**
     * Prepare existing row data object
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $optionExtraAttr = [];
        $optionExtraAttr['option_' . $this->_getCountryRenderer()->calcOptionHash($row->getData('country'))] =
            'selected="selected"';
        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
