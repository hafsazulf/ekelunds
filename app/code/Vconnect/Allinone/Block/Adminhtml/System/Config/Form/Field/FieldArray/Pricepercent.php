<?php
/* 
 * The MIT License
 *
 * Copyright 2016 vConnect.dk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * @category Magento
 * @package Vconnect_AllInOne
 * @author vConnect
 * @email kontakt@vconnect.dk
 * @class Prices
 */

namespace Vconnect\Allinone\Block\Adminhtml\System\Config\Form\Field\FieldArray;

use Magento\Backend\Block\Template\Context;

class Pricepercent extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    protected $_operationRenderer;

    /**
     * @var \Vconnect\Allinone\Model\System\Config\Source\Operation
     */
    protected $operation;

    /**
     * @var \Vconnect\Allinone\Helper\Data
     */
    protected $allinoneHelperData;

    /**
     * Priceforcountry constructor.
     * @param Context $context
     * @param \Vconnect\Allinone\Model\System\Config\Source\Operation $operation
     * @param \Vconnect\Allinone\Helper\Data $allinoneHelperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Vconnect\Allinone\Model\System\Config\Source\Operation $operation,
        \Vconnect\Allinone\Helper\Data $allinoneHelperData,
        array $data = []
    ) {
        $this->operation = $operation;
        $this->allinoneHelperData = $allinoneHelperData;

        parent::__construct($context, $data);

        $this->setHtmlId('_' . uniqid());
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $fieldConfig = $element->getFieldConfig();

        $countryCode = $this->allinoneHelperData->getStoreConfig('shipping/origin/country_id');

        if ($this->allinoneHelperData->canShowConfiguration($countryCode, $fieldConfig['id'])) {
            return parent::render($element);
        } else {
            return '';
        }
    }

    protected function _prepareToRender()
    {
        // create columns for the table rate for the carriers(other than pickup) system config 
        $this->addColumn('orderminprice', array(
            'label' => __('Min Price'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));
        $this->addColumn('ordermaxprice', array(
            'label' => __('Max Price'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));
        $this->addColumn('operation', array(
            'label' => __('Operation'),
            'size' => 6,
            'style' => 'width: 10%;',
            'renderer' => $this->_getOperationRenderer()
        ));
        $this->addColumn('percent', array(
            'label' => __('Percent'),
            'size' => 6,
            'style' => 'width: 95%;',
        ));

        $this->_addAfter = false;
    }

    /**
     * Retrieve group column renderer
     *
     * @return Serviceslabels|\Magento\Framework\View\Element\BlockInterface
     */
    protected function _getOperationRenderer()
    {
        if (!$this->_operationRenderer) {
            $this->_operationRenderer = $this->getLayout()->createBlock(
                'Magento\Framework\View\Element\Html\Select',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->_operationRenderer->setId('allinone_method_home_operation_row_<%- _id %>');
            $this->_operationRenderer->setName($this->getElement()->getName() . '[<%- _id %>][operation]');
            $this->_operationRenderer->setOptions($this->operation->toOptionArray());
        }

        return $this->_operationRenderer;
    }

    /**
     * Prepare existing row data object
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $optionExtraAttr = [];
        $optionExtraAttr['option_' . $this->_getOperationRenderer()->calcOptionHash($row->getData('operation'))] =
            'selected="selected"';
        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
