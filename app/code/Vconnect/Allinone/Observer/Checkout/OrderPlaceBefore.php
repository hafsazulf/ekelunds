<?php
/* 
 * The MIT License
 *
 * Copyright 2016 vConnect.dk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * @category Magento
 * @package Vconnect_AllInOne
 * @author vConnect
 * @email kontakt@vconnect.dk
 * @class Vconnect_AllInOne_Observer_Checkout_OrderPlaceBefore
 */

namespace Vconnect\Allinone\Observer\Checkout;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;

class OrderPlaceBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Vconnect\Allinone\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * SubmitAllAfter constructor.
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Vconnect\Allinone\Helper\Data $dataHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct (
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Vconnect\Allinone\Helper\Data $dataHelper,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->dataHelper = $dataHelper;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Magento\Framework\Exception\LocalizedException
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $order = $observer->getevent()->getorder();
        $quote = $this->checkoutSession->getQuote();

        if (!$this->dataHelper->getStoreConfig('carriers/vconnectpostnord/active')){
            return;
        }

        if (stripos($order->getShippingMethod(), 'vconnectpostnord') === false || !$quote->getVconnectPostnordData()) {
            return;
        }

        $data = $this->jsonHelper->jsonDecode($quote->getVconnectPostnordData());

        if (strpos($order->getShippingMethod(), 'pickupinshop') === false && strpos($order->getShippingMethod(), 'pickup') !== false && !empty($data) && (empty($data['data']) || empty($data['data']['servicePointId']))) {
            throw new LocalizedException(
                new Phrase(__('Error happened with the shipping information - please choose the pickup point again.'))
            );
        }
    }
}
