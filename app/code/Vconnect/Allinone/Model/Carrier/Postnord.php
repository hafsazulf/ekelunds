<?php
/* 
 * The MIT License
 *
 * Copyright 2016 vConnect.dk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * @category Magento
 * @package Vconnect_AllInOne
 * @author vConnect
 * @email kontakt@vconnect.dk
 * @class Vconnect_AllInOne_Model_Carrier_Postnord
 */

namespace Vconnect\Allinone\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;

class Postnord extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    protected $_code = 'vconnectpostnord';

    protected $_code_method = 'vconnect_';

    protected $_rateResultFactory;

    protected $_rateMethodFactory;

    /**
     * @var HelperData
     */
    protected $_dataHelper;

    /**
     * @var \Vconnect\Allinone\Api\RateRepositoryInterface
     */
    protected $_allinoneRateRepository;

    /**
     * @var \Vconnect\Allinone\Api\Data\RateInterfaceFactory
     */
    protected $_allinoneRateFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfiguration;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_backendSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * @var \Magento\Framework\Data\Collection
     */
    protected $_dataCollection;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Vconnect\Allinone\Model\Apiclient
     */
    protected $_vconnectApiclient;

    /**
     * Postnord constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Vconnect\Allinone\Helper\Data $dataHelper
     * @param \Vconnect\Allinone\Api\RateRepositoryInterface $allinoneRateRepository
     * @param \Vconnect\Allinone\Api\Data\RateInterfaceFactory $allinoneRateFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfiguration
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Backend\Model\Session $backendSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Framework\Data\Collection $dataCollection
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Vconnect\Allinone\Model\Apiclient $vconnectApiclient
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Vconnect\Allinone\Helper\Data $dataHelper,
        \Vconnect\Allinone\Api\RateRepositoryInterface $allinoneRateRepository,
        \Vconnect\Allinone\Api\Data\RateInterfaceFactory $allinoneRateFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfiguration,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\State $state,
        \Magento\Store\Model\StoreManagerInterface $dataCollection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Vconnect\Allinone\Model\Apiclient $vconnectApiclient,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_dataHelper = $dataHelper;
        $this->_allinoneRateRepository = $allinoneRateRepository;
        $this->_allinoneRateFactory = $allinoneRateFactory;
        $this->_productRepository = $productRepository;
        $this->_scopeConfiguration = $scopeConfiguration;
        $this->_checkoutSession = $checkoutSession;
        $this->_backendSession = $backendSession;
        $this->_storeManager = $storeManager;
        $this->_state = $state;
        $this->_dataCollection = $dataCollection;
        $this->_objectManager = $objectManager;
        $this->_vconnectApiclient = $vconnectApiclient;

        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        $countryId = $this->_dataHelper->getStoreConfigFlag('shipping/origin/country_id') ? strtolower($this->_dataHelper->getStoreConfig('shipping/origin/country_id')) : '';
        return array(
            $countryId . '_privatehome'   => $this->_dataHelper->getStoreConfig('carrier/vconnect_postnord/title') . ' ' . __('privatehome'),
            $countryId . '_commercial'    => $this->_dataHelper->getStoreConfig('carrier/vconnect_postnord/title') . ' ' . __('commercial'),
            $countryId . '_pickup'        => $this->_dataHelper->getStoreConfig('carrier/vconnect_postnord/title') . ' ' . __('pickup'),
            $countryId . '_valuemail'     => $this->_dataHelper->getStoreConfig('carrier/vconnect_postnord/title') . ' ' . __('valuemail'),
            $countryId . '_pickupinshop'  => $this->_dataHelper->getStoreConfig('carrier/vconnect_postnord/title') . ' ' . __('pickupinshop'),
        );
    }

    /**
     * Collect and get rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        if(!$request->getCountryId()){
            return $this->_logger->debug('PostNord: No origin country');
        }

        if (!$request->getDestCountryId()) {
            return $this->_logger->debug('PostNord: No Destination country');
        }

        // exclude Virtual products price from Package value if pre-configured
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValueWithDiscount() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValueWithDiscount() - $item->getBaseRowTotal());
                }
            }
        }

        // Free shipping by qty
        $freeQty = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeQty += $item->getQty() * ($child->getQty() - (is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0));
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeQty += ($item->getQty() - (is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0));
                }
            }
        }

         // Package weight and qty free shipping
        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        $request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        $result = $this->_rateResultFactory->create();

        $methods = array();

        if (strtolower($request->getCountryId()) == 'dk') {
            $methods = $this->_dataHelper->collectShippingMethodsForDK($request->getDestCountryId());
        } elseif (strtolower($request->getCountryId()) == 'se') {
            $methods = $this->_dataHelper->collectShippingMethodsForSE($request->getDestCountryId());
        } elseif (strtolower($request->getCountryId()) == 'no') {
            $methods = $this->_dataHelper->collectShippingMethodsForNO($request->getDestCountryId());

            if (strtolower($request->getDestCountryId()) == 'no' && $this->_dataHelper->getStoreConfig('carriers/vconnectpostnord/norwegian_api_validation')) {
                if (!empty($request->getDestPostcode())) {
                    $allowed_methods = $this->_vconnectApiclient->getNOAvailableProducts($this->_checkoutSession->getQuote(), $request, \Vconnect\Allinone\Helper\Data::POSTNORD_NO_API_CUSTOMERID, \Vconnect\Allinone\Helper\Data::POSTNORD_NO_API_USERNAME, \Vconnect\Allinone\Helper\Data::POSTNORD_NO_API_PASSWORD);

                    if (empty($allowed_methods['postnord']['error']) && !empty($allowed_methods['postnord']['products'])) {
                        foreach ($methods as $key => $method) {
                            foreach ($allowed_methods['postnord']['products'] as $allowed_method) {
                                if ($allowed_method['productCode'] == $method->getProductCode()) {
                                    $method->setPostnordMethod($allowed_method);
                                    continue 2;
                                }
                            }

                            unset($methods[$key]);
                        }
                    } else {
                        unset($methods);
                    }
                } else {
                    unset($methods);
                }
            }
        } elseif (strtolower($request->getCountryId()) == 'fi') {
            $methods = $this->_dataHelper->collectShippingMethodsForFI($request->getDestCountryId());
        }

        if(empty($methods)){
            return false;
        }

        usort($methods, function($a,$b){
            if (isset($a['system_path']) && isset($b['system_path'])) {
                $a['sort_order'] = (int)$this->_dataHelper->getStoreConfig("carriers/{$a['system_path']}/sort_order");
                $b['sort_order'] = (int)$this->_dataHelper->getStoreConfig("carriers/{$b['system_path']}/sort_order");
                if ($a['sort_order'] == $b['sort_order']) {
                    return 0;
                }
                return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
            } else {
                return 0;
            }
        });

        $allVconnectAllinoneMethodData = array();
        foreach ($methods as $_method) {
            if (strtolower($request->getDestCountryId()) == 'no' && strtolower($request->getCountryId()) == 'no' && $this->_dataHelper->getStoreConfig('carriers/vconnectpostnord/norwegian_api_validation')) {
                $method = $this->_createShippingMethodByNOAPI($request, $freeQty, $_method);
            } else {
                $method = $this->_createShippingMethodByCode($request, $freeQty, $_method);
            }

            if (!$method) {
                continue;
            }

            $allinoneMethodData = array();
            if ($method->hasVcMethodData()) {
                $vconnectPostnordData = json_decode($method->getVcMethodData());
                $allVconnectAllinoneMethodData[$vconnectPostnordData->system_path] = $method->getData();
                $allVconnectAllinoneMethodData[$vconnectPostnordData->system_path]['code'] = $this->getCarrierCode() . '_' . $vconnectPostnordData->method;
                $allVconnectAllinoneMethodData[$vconnectPostnordData->system_path]['sort_order'] = $vconnectPostnordData->sort_order;
            }

            $result->append($method);
        }

        if (!empty($allVconnectAllinoneMethodData)) {
            $magentoDateObject = $this->_objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');

            $allinoneRate = $this->_allinoneRateFactory->create();

            $collection = $allinoneRate->getCollection();
            $collection->addFieldToFilter('quote_id', array('eq' => $this->_checkoutSession->getQuote()->getId()));
            $collection->addFieldToFilter('address_id', array('eq' => $this->_checkoutSession->getQuote()->getShippingAddress()->getId()));

            $isRowExist = false;
            foreach ($collection as $item) {
                $isRowExist = true;
                $item->setRateData(serialize($allVconnectAllinoneMethodData));
                $item->save();
            }

            if (!$isRowExist) {
                $allinoneRate
                    ->setRateData(serialize($allVconnectAllinoneMethodData))
                    ->setQuoteId($this->_checkoutSession->getQuote()->getId())
                    ->setAddressId($this->_checkoutSession->getQuote()->getShippingAddress()->getId())
                    ->setDateCreated($magentoDateObject->gmtDate());
                    $this->_allinoneRateRepository->save($allinoneRate);
            }
        }

        return $result;
    }

    /**
     * Get price rate for order with specific weight and subtotal
     * @param float $orderPrice
     * @param float $orderWeight
     * @param \Magento\Framework\DataObject $config method configuration
     * @return float
     */
    public function getRate($orderPrice, $orderWeight, \Magento\Framework\DataObject $config, $filter = array())
    {
        $code = $config->getSystemPath();

        if($config->getMultiprices()){
            $ratePath = sprintf('carriers/%s/price_%s',$code, $config->getDestCountry());
        }else{
            $ratePath = sprintf('carriers/%s/%s',$code, $config->getPriceCode());
        }

        $result = $this->_dataHelper->getStoreConfig($ratePath);
        if(!$result){
            $this->_logger->debug("no rate for code $code and path $ratePath" );
            
            return FALSE;
        }
        $pickupShippingRates = $this->_dataHelper->unserialize($result);

        if (is_array($pickupShippingRates)) {
            foreach ($filter as $filterField => $filterValue) {
                foreach ($pickupShippingRates as $rateKey => $rate) {
                    if (isset($rate[$filterField]) && strtolower($rate[$filterField]) != strtolower($filterValue)) {
                        unset($pickupShippingRates[$rateKey]);
                    }
                }
            }

            if (!empty($pickupShippingRates)) {
                foreach ($pickupShippingRates as $pickupShippingRate) {
                    if( (float)$pickupShippingRate['orderminprice'] <= (float)$orderPrice
                            && ( (float)$pickupShippingRate['ordermaxprice'] >= (float)$orderPrice || (float)$pickupShippingRate['ordermaxprice'] == 0)
                            && (float)$pickupShippingRate['orderminweight'] <= (float)$orderWeight
                            && ( (float)$pickupShippingRate['ordermaxweight'] >= (float)$orderWeight || (float)$pickupShippingRate['ordermaxweight'] == 0)
                            ) {
                        return $pickupShippingRate;
                    }
                }
            }
        }

        return FALSE;
    }

    /**
     * Get price rate for order with specific weight and subtotal
     * @param float $orderPrice
     * @param float $orderWeight
     * @param DataObject $config method configuration
     * @return float
     */
    public function getRateForMethodHome($orderPrice, $orderWeight, \Magento\Framework\DataObject $config) 
    {
        $code = $config->getSystemPath();

        if($config->getMultiprices()){
            $ratePath = sprintf('carriers/%s/price_%s',$code, $config->getDestCountry());
        }else{
            $ratePath = sprintf('carriers/%s/%s',$code, $config->getPriceCode());
        }

        $result = $this->_dataHelper->getStoreConfig($ratePath);
        if(!$result){
            $this->_logger->debug("no rate for code $code and path $ratePath");
            return FALSE;
        }

        $homeShippingRates = $this->_dataHelper->unserialize($result); 
        if (is_array($homeShippingRates) && !empty($homeShippingRates)) {
            foreach ($homeShippingRates as $homeShippingRate) {
                if(strtoupper($config->getDestCountry()) == $homeShippingRate['country']
                        && (float)$homeShippingRate['orderminprice'] <= (float)$orderPrice
                        && ( (float)$homeShippingRate['ordermaxprice'] >= (float)$orderPrice || (float)$homeShippingRate['ordermaxprice'] == 0)
                        && (float)$homeShippingRate['orderminweight'] <= (float)$orderWeight
                        && ( (float)$homeShippingRate['ordermaxweight'] >= (float)$orderWeight || (float)$homeShippingRate['ordermaxweight'] == 0)
                        ) {
                    return $homeShippingRate;
                }
            }
        }
        return FALSE;
    }

    /**
     * Get price with added/subtracted percentage
     * @param float $orderPrice
     * @param string $config
     * @return float
     */
    public function getPercentPrice($orderPrice, $config)
    {
        $data = $this->_dataHelper->getStoreConfig($config);

        if (!empty($data) ) {
            $rates = $this->_dataHelper->unserialize($data);

            if (is_array($rates)) {
                if (!empty($rates)) {
                    foreach ($rates as $rate) {
                        if ((float)$rate['orderminprice'] <= (float)$orderPrice
                                && ( (float)$rate['ordermaxprice'] >= (float)$orderPrice || (float)$rate['ordermaxprice'] == 0)
                                ) {
                            if ($rate['operation'] == '+') {
                                return $orderPrice + (($orderPrice / 100) * $rate['percent']);
                            } else {
                                return $orderPrice - (($orderPrice / 100) * $rate['percent']);
                            }
                        }
                    }
                }
            }
        }

        return $orderPrice;
    }

    /**
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     * @param string $code
     * @param float $freeQty
     * @param array $data Method data
     * @param Vconnect_AllInOne_Model_Carrier_Postnord $carrier carrier object
     * @return Mage_Shipping_Model_Rate_Result_Method|Bool
     */
    protected function _createShippingMethodByCode(RateRequest $request, $freeQty, \Magento\Framework\DataObject $data)
    {
        $code = $data->getSystemPath();
        $methodCode = $data->getMethod();

        if( !$this->_dataHelper->getStoreConfig("carriers/$code/active") ){
            return false;
        }

        $total = ($request->getBaseSubtotalInclTax() !== null ? $request->getBaseSubtotalInclTax() : $request->getPackageValueWithDiscount());

        $data->setCountry(strtolower($request->getCountryId()));
        $data->setDestCountry(strtolower($request->getDestCountryId()));
        $data->setSortOrder((int)$this->_dataHelper->getStoreConfig("carriers/$code/sort_order"));

        if ($data->getSystemPath() == 'vconnect_postnord_home') {
            $rate = $this->getRateForMethodHome($total, $request->getPackageWeight(), $data);

            // Option text method data
            $configOptionText = clone $data;
            $configOptionText->setPriceCode('arrival_option_text_price');
            $rateOptionText = $this->getRateForMethodHome($total, $request->getPackageWeight(), $configOptionText);
            if ($rateOptionText !== false) {
                $optionTextData = array(
                    'label'             => $this->_dataHelper->getStoreConfig("carriers/$code/arrival_option_text"),
                    'label_with_price'  => $this->_dataHelper->getStoreConfig("carriers/$code/arrival_option_text") . ' - ' . $this->_dataHelper->getPriceFormated($this->_checkoutSession->getQuote(), $rateOptionText['price'], true, false),
                    'price'             => $this->_dataHelper->getPriceFormated($this->_checkoutSession->getQuote(), $rateOptionText['price'], true, false),
                    'base_price'        => $rateOptionText['price'],
                );
                $data->setOptionTextData($optionTextData);
            }

            // Additional fee method data
            if ($this->_dataHelper->getStoreConfigFlag("carriers/$code/additional_fee_active")) {
                $configAdditionalFee = clone $data;
                $configAdditionalFee->setPriceCode('additional_fee_amount');
                $rateAdditionalFee = $this->getRateForMethodHome($total, $request->getPackageWeight(), $configAdditionalFee);
                if ($rateAdditionalFee !== false) {
                    $additionalFeeData = array(
                        'label'             => $this->_dataHelper->getStoreConfig("carriers/$code/additional_fee_label"),
                        'label_with_price'  => $this->_dataHelper->getStoreConfig("carriers/$code/additional_fee_label") . ' - ' . $this->_dataHelper->getPriceFormated($this->_checkoutSession->getQuote(), $rateAdditionalFee['price'], true, false),
                        'price'             => $this->_dataHelper->getPriceFormated($this->_checkoutSession->getQuote(), $rateAdditionalFee['price'], true, false),
                        'base_price'        => $rateAdditionalFee['price'],
                    );
                    $data->setAdditionalFeeData($additionalFeeData);
                }
            }

            // Flex delivery data
            if ($rate !== false) {
                $flexDeliveryData = array(
                    'label'             => $this->_dataHelper->getStoreConfig("carriers/$code/flex_delivery_label"),
                    'label_with_price'  => $this->_dataHelper->getStoreConfig("carriers/$code/flex_delivery_label") . ' - ' . $this->_dataHelper->getPriceFormated($this->_checkoutSession->getQuote(), $rate['price'], true, false),
                    'price'             => $this->_dataHelper->getPriceFormated($this->_checkoutSession->getQuote(), $rate['price'], true, false),
                    'base_price'        => $rate['price'],
                );
                $data->setFlexDeliveryData($flexDeliveryData);
            }

            $data->setDeliveryTime($this->_dataHelper->getStoreConfig("carriers/$code/transit_time")?:false);
            $method_title = $this->_dataHelper->getStoreConfig("carriers/$code/name");
            $method_description = $this->_dataHelper->getStoreConfig("carriers/$code/description");

            $vconnectPostnordData = $this->_checkoutSession->getQuote()->getVconnectPostnordData();
            if ($vconnectPostnordData) {
                $vconnectPostnordData = json_decode($vconnectPostnordData);
                if (isset($vconnectPostnordData->additional_fee_amount)) {
                    $rate['price'] = $vconnectPostnordData->additional_fee_amount;
                }
            }
        } elseif ($data->getSystemPath() == 'vconnect_postnord_pickup') {
            $rate = $this->getRate($total, $request->getPackageWeight(), $data, array('country' => $data->getDestCountry()));

            $data->setDeliveryTime($rate['transit_time']);
            $method_title = $rate['name'];
            $method_description = $rate['description'];
        } else {
            $rate = $this->getRate($total, $request->getPackageWeight(), $data);

            $data->setDeliveryTime($this->_dataHelper->getStoreConfig("carriers/$code/transit_time")?:false);
            $method_title = $this->_dataHelper->getStoreConfig("carriers/$code/name");
            $method_description = $this->_dataHelper->getStoreConfig("carriers/$code/description");
        }

        $method_description = $this->_dataHelper->getStoreConfig("carriers/$code/description");

        if ($rate === false) {
            $this->_logger->debug("No price rate for $code");
            return false;
        }
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->getCarrierCode());
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setVcMethodData($data->toJson());
        $method->setMethod($methodCode);
        $method->setMethodTitle($method_title);
        $method->setMethodDescription($method_description);

        if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
            $shippingPrice = 0;
        } else {
            $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
        }

        $method->setPrice($shippingPrice)->setCost($rate['price']);
        
        return $method;
    }

    /**
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     * @param string $code
     * @param float $freeQty
     * @param array $data Method data
     * @param Vconnect_AllInOne_Model_Carrier_Postnord $carrier carrier object
     * @return Mage_Shipping_Model_Rate_Result_Method|Bool
     */
    protected function _createShippingMethodByNOAPI(RateRequest $request, $freeQty, \Magento\Framework\DataObject $data)
    {
        $code = $data->getSystemPath();
        $methodCode = $data->getMethod();

        if (!$this->_dataHelper->getStoreConfig("carriers/$code/active")) {
            return false;
        }

        $data->setCountry(strtolower($request->getCountryId()));
        $data->setDestCountry(strtolower($request->getDestCountryId()));
        $data->setSortOrder((int)$this->_dataHelper->getStoreConfig("carriers/$code/sort_order"));
        $postnordMethodData = $data->getPostnordMethod();

        if ($this->_dataHelper->getStoreConfig("carriers/$code/price_percent")) {
            if (!empty($postnordMethodData['additionalServices'])) {
                foreach ($postnordMethodData['additionalServices'] as $key => $additionalService) {
                    $postnordMethodData['additionalServices'][$key]['priceBreakdown']['netAmount']['value'] = $this->getPercentPrice($additionalService['priceBreakdown']['netAmount']['value'], "carriers/$code/service_price_percent");
                }
            }

            $price = $this->getPercentPrice($postnordMethodData['price']['priceBreakdown']['netAmount']['value'], "carriers/$code/shipping_price_percent");
        }

        $datetime1 = date_create($postnordMethodData['expectedDelivery']['date']);
        $datetime2 = date_create(date('Y-m-d'));

        $interval = date_diff($datetime1, $datetime2);

        $data->setDeliveryTime($interval->format('%d') . ' ' . __('days'));
        $data->setPostnordMethod($postnordMethodData);
        $data->setServiceTitle($this->_dataHelper->getStoreConfig("carriers/$code/service_title"));

        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->getCarrierCode());
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setVcMethodData($data->toJson());
        $method->setMethod($methodCode);
        $method->setMethodTitle($postnordMethodData['displayName']);
        $method->setMethodDescription($postnordMethodData['texts']['descriptionText']);

        if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
            $shippingPrice = 0;
        } else {
            $shippingPrice = $this->getFinalPriceWithHandlingFee($price);
        }

        $method->setPrice($shippingPrice)->setCost($price);

        return $method;
    }
}
