<?php

namespace Coderzone\ProductGiftbox\Model\Source;

class GiftProducts extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource 
{
    public function getAllOptions() {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('Select Gift Product'), 'value' => ''],
                ['label' => __('Gift Box Product 1'), 'value' => 'gift-box-1'],
                ['label' => __('Gift Box Product 2'), 'value' => 'gift-box-2']
            ];
        }
        return $this->_options;
    }
}