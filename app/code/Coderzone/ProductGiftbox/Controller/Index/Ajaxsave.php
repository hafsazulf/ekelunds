<?php
namespace Coderzone\ProductGiftbox\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Ajaxsave extends Action
{
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
        )
    {
       parent::__construct($context);
       $this->cart = $cart;
       $this->resultJsonFactory = $resultJsonFactory;
       $this->_productRepository = $productRepository;
    }


    public function execute()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $qty = $this->getRequest()->getParam('qty');
		if($qty)
        {
			try{
				$product = $this->_productRepository->getById($productId);
				$params = array(
					'product' => $product->getId(),
					'qty' => $qty
				);
				$this->cart->addProduct($product,$params);
				$this->cart->getQuote()->setTotalsCollectedFlag(false);
				$this->cart->save();
				if (!$this->cart->getQuote()->getHasError()) {
					$message = __(
					'You have added Gift Box in to cart'
					);
				    $response = ['message' => $message,'class' => 'message-success success message'];
				}
			}catch(\Exception $e){
				$response = ['message' => $e->getMessage(),'class' => 'message-error error message'];
			}
		}else{
			$message = __('Qty less then 1 is not allow');
			$response = ['message' => $message,'class' => 'message-error error message'];
		}
        $dataJson = $this->resultJsonFactory->create();
        return $dataJson->setData($response);
    }
}