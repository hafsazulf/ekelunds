<?php
namespace Ekelund\Common\Cron;

class Catmodel
{
	protected $_catmodel;
	protected $_catcollection;
	protected $_cateinstance;
	
	public function __construct(
		\Ekelund\Common\Model\Catmodel $catmodel,
		\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $catcollection,
		\Magento\Catalog\Model\CategoryFactory $cateinstance
    ) {
		$this->_catmodel = $catmodel;
		$this->_catcollection = $catcollection;
		$this->_cateinstance = $cateinstance;
    }

	public function execute()
	{
		$categories = $this->_catcollection->create();
		$categories->addAttributeToSelect('display_mode');
		$cateinstance = $this->_cateinstance->create();
		foreach ($categories as $category) {
			if($category->getId()>2){
				$productcount = $cateinstance->create()->load($category->getId())->getProductCount();
				if($category->getLevel()>2 && $category->getDisplayMode() != 'PAGE'){
					$catmodel = $obj->get('Ekelund\Common\Model\Catmodel');
					$catitem = $catmodel->getCollection()->addFieldToFilter('category_id',$category->getId())->getFirstItem();
					if($catitem->getId()){
						$catmodel->load($catitem->getId())->setProductCount($productcount)->save();
					}else{
						$catmodel->setId(null)->setProductCount($productcount)->setCategoryId($category->getId())->save();
					}
				}
			}
		}
	}
}