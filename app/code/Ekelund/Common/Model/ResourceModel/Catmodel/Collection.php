<?php
namespace Ekelund\Common\Model\ResourceModel\Catmodel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ekelund\Common\Model\Catmodel', 'Ekelund\Common\Model\ResourceModel\Catmodel');
    }
}
?>