<?php
namespace Ekelund\Common\Model\ResourceModel;

class Catmodel extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('category_product_count', 'id');
    }
}
?>