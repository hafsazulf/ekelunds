<?php
namespace Ekelund\Common\Model;


class Catmodel extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ekelund\Common\Model\ResourceModel\Catmodel');
    }
}
