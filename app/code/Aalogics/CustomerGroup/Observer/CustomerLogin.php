<?php

namespace Aalogics\CustomerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\GroupFactory;

class CustomerLogin implements ObserverInterface
{
	protected $_customerSession;

	protected $_storeManager;

    protected $_groupFactory;

    
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        GroupFactory $groupFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_groupFactory = $groupFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerGroupId = $this->_customerSession->getCustomer()->getGroupId();

        $collection = $this->_groupFactory->create()->getCollection()->addFieldToFilter('customer_group_id',array('eq' => $customerGroupId))->getData();

   
        if(!empty($collection[0]['group_currency']))
        {
        	$currency = $collection[0]['group_currency'];
        	$this->_storeManager->getStore()->setCurrentCurrencyCode($currency);
        }

    }
}