<?php

namespace Aalogics\CustomerGroup\Plugin;

use Magento\Customer\Controller\RegistryConstants;

class EditPlugin
{
    protected $_groupFactory;
    protected $_coreRegistry;

    public function __construct(
        \Magento\Customer\Model\GroupFactory $groupFactory,
        \Magento\Framework\Registry $coreRegistry       
    ){
        $this->_groupFactory = $groupFactory;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Get form HTML
     *
     * @return string
     */
    public function aroundGetFormHtml(
        \Magento\Customer\Block\Adminhtml\Group\Edit\Form $subject,
        \Closure $proceed
    )
    {
        $groupId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_GROUP_ID);
        $customerGroup = $this->_groupFactory->create();
        $customerGroup->load($groupId);

        $form = $subject->getForm();
        if (is_object($form)) {
            $fieldset = $form->getElement('base_fieldset');
            $fieldset->addField(
                'group_currency',
                'text',
                [
                    'name' => 'group_currency',
                    'label' => __('Group Currency'),
                    'title' => __('Group Currency'),
                    'value' => $customerGroup->getData('group_currency'),
                    'required' => false
                ]
            );
            
            $subject->setForm($form);
        }

        return $proceed();
    }
}