var config = {
    config: {
        mixins: {
            'Magento_ConfigurableProduct/js/configurable': {
                'Ekologiskt_ShowVariousSKU/js/model/skuswitch': true
            },
            'Magento_Swatches/js/swatch-renderer': {
                'Ekologiskt_ShowVariousSKU/js/model/swatch-skuswitch': true
            }
        }
    }
};